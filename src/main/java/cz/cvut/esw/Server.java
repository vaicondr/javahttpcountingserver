package cz.cvut.esw;

import org.apache.commons.io.IOUtils;
import org.rapidoid.http.Req;
import org.rapidoid.setup.App;
import org.rapidoid.setup.On;
import org.rapidoid.u.U;

import java.io.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.zip.GZIPInputStream;

public class Server {
    private static final String postPath = "/esw/myserver/data";
    private static final String getPath = "/esw/myserver/count";
    private static final String charset = "utf-8";

    private static ConcurrentHashMap<String, Integer> dictionary = new ConcurrentHashMap<>();
    public void runServer(String[] args) {
        App.init(args);
        On.defaults().managed(false);
        On.get(getPath).plain(() -> U.str(getUniqueCount()));
        On.post(postPath).plain(this::postHandler);
        App.ready();
    }

    private String postHandler(Req req) {
        try(GZIPInputStream gzipIn = new GZIPInputStream(new ByteArrayInputStream(req.body()))) {
            String content = IOUtils.toString(gzipIn, charset);
            String cleanedContent = content.trim();
            String[] words =  cleanedContent.split("\\s+");
            for ( String word : words) {
                dictionary.put(word,1);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String getUniqueCount() {
        int amount = dictionary.values().size();
        dictionary.clear();
        return amount + "";
    }
}
